import Blog from "./Blog";
const BlogList = ( {blogs, title, handleDelete} ) => {


    let blogList = blogs.map( blog => 
        <Blog title={blog.title} 
            author={blog.author} 
            id={blog.id}
            key={blog.id}
            handleDelete={handleDelete}
             />
    );


    return (
        <div className="home">
            <h2>{title}</h2>
            {blogList}
        </div>
    );
}

export default BlogList;