import { useEffect, useState } from "react";
import BlogList from "./BlogList";

const Home = () => {

    // const [blogs, setBlogs] = useState([
    //     {   title: "Step 1",
    //         body: "Less time for interaction",
    //         author: "mayhxm",
    //         id: 1
    // },
    // {   title: "Step 2",
    //         body: "Remove certain special memories",
    //         author: "mayhxm",
    //         id: 2
    // },
    // {   title: "Step 3",
    //         body: "Keep it that way.",
    //         author: "mayhxm",
    //         id: 3
    // }
    // ]);

    const [blogs, setBlogs] = useState([]);

   // const [name, setName] = 
    const handleDelete = (id) => {

      const newBlogs = blogs.filter( blog => blog.id != id );
      setBlogs(newBlogs);

  };

  useEffect(() => {

    const blogs = fetch('http://localhost:8000/blogs')
      .then( res => {
        return res.json()
      })
      .then( (data) => {
        setBlogs(data);
        console.log(data);
      })
  }, []);

    return (
        <div className="home">
           <BlogList blogs={blogs} title="Recent blogs" handleDelete={handleDelete} />
        </div>
      );
}
 
export default Home;