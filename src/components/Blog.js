const Blog = ( {
    title,
    author,
    id,
    handleDelete
}) => {
    return (  
        <div className="blog-preview">
            <h2>{title}</h2>
            <p> Written by : {author}</p>
            <button onClick={ () => handleDelete(id)}>Delete entry</button>
        </div>
    );
}
 
export default Blog;